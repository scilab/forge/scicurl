// Copyright (C) 2011 - Sylvestre LEDRU
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function builder_c()

  src_c_path = get_absolute_file_path("builder_c.sce");
  CFLAGS = '';
  LDFLAGS = '';

  files_c = "dlManager.c";
  
  if getos() <> "Windows" then
    CFLAGS = "-I/usr/include/libxml2/ -I" + src_c_path;
    LDFLAGS = '-lcurl';
  else
    CFLAGS = "-I" + fullpath(src_c_path + "/../../thirdparty/curl/include/") + ..
             " -I" + fullpath(src_c_path + "/../../thirdparty/libxml2/include/") + ..
             " -I" + fullpath(src_c_path);
    files_c = [files_c; ..
               "dllMain.c"; ..
               "isdir.c"; ..
               "isDrive.c"];
  end

  tbx_build_src("curllib",        ..
              files_c,            ..
              "c", ..             ..
              src_c_path,         ..
              "",                 ..
              LDFLAGS,..
              CFLAGS);

endfunction

builder_c();
clear builder_c;
