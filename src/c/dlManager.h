/* ==================================================================== */
/* Template toolbox_curl */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */
#ifndef __DLMANAGER_H__
#define __DLMANAGER_H__

#include <stdio.h>
#include <stdlib.h>
#include "BOOL.h"

#define DEFAULT_FILENAME "index.html"

/**
 * Download a file
 * @param[in] fileUrl the location of the file 
 * @param[in] dest where to save the file
 * @return the file pointer
*/
char *downloadFile(char *url, char *dest, char *username, char *password);

/**
 * Download the content of an URL
 * @param[in] Url the location
 * @return the content
 */
char *downloadContent(char *url, char *username, char *password);

#endif /* __DLMANAGER_H__ */
