// Copyright (C) 2011 - Sylvestre LEDRU
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

LDFLAGS = '';
CFLAGS = ''

if getos() == "Windows" then
    // to manage long pathname
    includes_src_c = "-I""" + fullpath(get_absolute_file_path("builder_gateway_c.sce") + "../../src/c") + """";
    CFLAGS = includes_src_c + " -I" + fullpath(get_absolute_file_path("builder_gateway_c.sce") + "../../thirdparty/libxml2/include/");
else
    includes_src_c = "-I" + get_absolute_file_path("builder_gateway_c.sce") + "../../src/c  -I/usr/include/libxml2/";
    CFLAGS = includes_src_c;
    LDFLAGS = "-lcurl -lxml2 -g";
end

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

functions_gw = ["getURL","sci_getURL"; ..
                "splitURL","sci_splitURL"; ..
                "getURLcontent","sci_getURLcontent"];
                
functions_files = ["sci_getURL.c", ..
                   "sci_splitURL.c", ..
                   "sci_getURLcontent.c"];

if getos() == "Windows" then
  functions_files = [functions_files, "dllMain.c"];
end

tbx_build_gateway("curl_gateway", ..
                  functions_gw, ..
                  functions_files, ..
                  get_absolute_file_path("builder_gateway_c.sce"), ..
                  ["../../src/c/libcurllib"], ..
                  LDFLAGS, ..
                  CFLAGS);

clear WITHOUT_AUTO_PUTLHSVAR;

clear tbx_build_gateway;
