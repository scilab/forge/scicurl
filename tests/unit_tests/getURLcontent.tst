// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - Sylvestre LEDRU
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

//getURLcontent

HTMLContent=getURLcontent("http://sylvestre.ledru.info:80");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"html") == [] then pause, end

HTMLContent=getURLcontent("http://plop:ae@sylvestre.ledru.info:80");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"html") == [] then pause, end

HTMLContent=getURLcontent("http://sylvestre.ledru.info/aze");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"404") == [] then pause, end



HTMLContent=getURLcontent("http://sylvestre.ledru.info");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"html") == [] then pause, end

HTMLContent=getURLcontent("http://sylvestre.ledru.info/");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"html") == [] then pause, end

HTMLContent=getURLcontent("ftp://ftp.free.fr/pub/Distributions_Linux/debian/README");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"Linux") == [] then pause, end

// HTTPS
HTMLContent=getURLcontent("https://encrypted.google.com");
if length(HTMLContent) == 0 then pause, end

HTMLContent=getURLcontent("http://httpbin.org/basic-auth/user/passwd","user","passwd");
if length(HTMLContent) == 0 then pause,end
if grep(HTMLContent,"authenticated") == [] then pause, end

// Wrong URL
if ~execstr("getURLContent(''http://plop@ae:sylvestre.ledru.info:80'')",'errcatch')<>0 then pause, end
