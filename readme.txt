SciCurl
=======

This module extends Scilab features by providing download remote files 
through HTTP, HTTPS and FTP (and probably other protocols).

It provides proxy support through the ATOMS config file.

It supports both IPv4 and IPv6.

This module is based on:
 * libcurl
 * libxml2 (for the URL parsing function)

It has been developped and tested under GNU/Linux Debian
with libxml2-dev 2.7.8.dfsg-3 and libcurl4-openssl-dev 7.21.6-3.

For a binary distribution, please note that libxml2 should NOT be shipped since
Scilab is embedding it for its needs.
While libcurl should come with ssl or gnutls library.

